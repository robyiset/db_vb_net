USE [master]
GO
/****** Object:  Database [mh02_copy]    Script Date: 19/11/2021 13:53:33 ******/
CREATE DATABASE [mh02_copy]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'mh02_copy', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\mh02_copy.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'mh02_copy_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\mh02_copy_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [mh02_copy] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [mh02_copy].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [mh02_copy] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [mh02_copy] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [mh02_copy] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [mh02_copy] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [mh02_copy] SET ARITHABORT OFF 
GO
ALTER DATABASE [mh02_copy] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [mh02_copy] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [mh02_copy] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [mh02_copy] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [mh02_copy] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [mh02_copy] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [mh02_copy] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [mh02_copy] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [mh02_copy] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [mh02_copy] SET  ENABLE_BROKER 
GO
ALTER DATABASE [mh02_copy] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [mh02_copy] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [mh02_copy] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [mh02_copy] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [mh02_copy] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [mh02_copy] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [mh02_copy] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [mh02_copy] SET RECOVERY FULL 
GO
ALTER DATABASE [mh02_copy] SET  MULTI_USER 
GO
ALTER DATABASE [mh02_copy] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [mh02_copy] SET DB_CHAINING OFF 
GO
ALTER DATABASE [mh02_copy] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [mh02_copy] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [mh02_copy] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [mh02_copy] SET QUERY_STORE = OFF
GO
USE [mh02_copy]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [mh02_copy]
GO
/****** Object:  Table [dbo].[tbl_t_confirmation]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_t_confirmation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[confirmation] [varchar](15) NOT NULL,
	[catatan] [text] NULL,
	[nama_nrp_gl] [varchar](50) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[id_monitor] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_t_monitor]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_t_monitor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[heart_beat] [float] NULL,
	[timestamp] [varchar](14) NOT NULL,
	[device_id] [varchar](8) NOT NULL,
	[device_ip] [varchar](15) NULL,
	[unit_no] [varchar](5) NOT NULL,
	[incident_code] [varchar](20) NOT NULL,
	[incident_start] [varchar](14) NOT NULL,
	[incident_end] [varchar](14) NULL,
	[is_visual_alert_on] [int] NOT NULL,
	[is_sound_alert_on] [int] NOT NULL,
	[image_file_name] [varchar](255) NULL,
	[video_file_name] [varchar](255) NULL,
	[gps_lat] [float] NULL,
	[gps_long] [float] NULL,
	[road_segment_id] [varchar](10) NOT NULL,
	[hm] [float] NULL,
	[rpm] [float] NULL,
	[vehicle_speed] [float] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_control_monitor_alarm]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_control_monitor_alarm] as
SELECT
	monitor.id,
	monitor.unit_no,
	'PAMA' as organisation,
	monitor.device_id,
	monitor.vehicle_speed,
	(case when ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) = 1 then 'Kesatu' 
	when ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) = 2 then 'Kedua'
	when ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) = 3 then 'Ketiga' else '' end) as frequency_incident,
	left(incident_start, 4) + '-' + 
	right(left(left(incident_start, len(incident_start)-6), len(left(incident_start, len(incident_start)-6))-2), 2) + '-' + 
	right(left(incident_start, len(incident_start)-6), 2) + ' ' +
	left(right(incident_start, 6), 2) + ':' + 
	right(left(incident_start, len(incident_start)-2), 2) + ':' + 
	right(incident_start, 2) as incident_start,
	monitor.incident_code,
	monitor.is_sound_alert_on + monitor.is_visual_alert_on as total_alarm,
	monitor.road_segment_id,
	(case when confirm.confirmation is null and 
	ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) > 0 then 'OPEN' else confirm.confirmation end) as confirmation
FROM 
	dbo.tbl_t_monitor as monitor 
left join (select 
id_monitor, confirmation, max(created_date) as created_date
from dbo.tbl_t_confirmation 
group by id_monitor, confirmation) as confirm on monitor.id = confirm.id_monitor;
GO
/****** Object:  View [dbo].[vw_confirmation_sum]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_confirmation_sum] as
select count(conf.confirmation) as total, conf.confirmation from
(SELECT
	(case when confirm.confirmation is null and 
	ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) > 0 then 'OPEN' else confirm.confirmation end) as confirmation
FROM 
	dbo.tbl_t_monitor as monitor 
left join (select 
id_monitor, confirmation, max(created_date) as created_date
from dbo.tbl_t_confirmation 
group by id_monitor, confirmation) as confirm on monitor.id = confirm.id_monitor) as conf
group by conf.confirmation


GO
/****** Object:  View [dbo].[vw_date_sum]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_date_sum] as
select count(incident_date) as total, incident_date
from (SELECT
	left(incident_start, 4) + '-' + 
	right(left(left(incident_start, len(incident_start)-6), len(left(incident_start, len(incident_start)-6))-2), 2) + '-' + 
	right(left(incident_start, len(incident_start)-6), 2) as incident_date
FROM 
	dbo.tbl_t_monitor) as datesum group by incident_date


GO
/****** Object:  View [dbo].[vw_sum_incident_code]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_sum_incident_code] as
SELECT
	count(incident_code) as total,
	incident_code
FROM 
	dbo.tbl_t_monitor group by incident_code;
GO
/****** Object:  View [dbo].[vw_log_pelanggaran]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_log_pelanggaran] as
SELECT
	incident_start,
	left(incident_start, 4) + '-' + 
	right(left(left(incident_start, len(incident_start)-6), len(left(incident_start, len(incident_start)-6))-2), 2) + '-' + 
	right(left(incident_start, len(incident_start)-6), 2) as incident_date,
	left(right(incident_start, 6), 2) + ':' + 
	right(left(incident_start, len(incident_start)-2), 2) + ':' + 
	right(incident_start, 2) as incident_time,
	incident_code,
	(case when ROW_NUMBER() OVER(PARTITION BY unit_no ORDER BY unit_no ASC) = 1 then 'Kesatu' 
	when ROW_NUMBER() OVER(PARTITION BY unit_no ORDER BY unit_no ASC) = 2 then 'Kedua'
	when ROW_NUMBER() OVER(PARTITION BY unit_no ORDER BY unit_no ASC) = 3 then 'Ketiga' else '' end) as frequency_incident,
	unit_no,
	road_segment_id
FROM 
	dbo.tbl_t_monitor;
GO
/****** Object:  View [dbo].[vw_report]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_report] as
SELECT
	monitor.id,
	monitor.unit_no,
	monitor.device_id,
	monitor.vehicle_speed,
	ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) as frequency_incident,
	convert(date, left(monitor.incident_start, 4) + '-' + 
	right(left(left(monitor.incident_start, len(monitor.incident_start)-6), len(left(monitor.incident_start, 
	len(monitor.incident_start)-6))-2), 2) + '-' + 
	right(left(monitor.incident_start, len(monitor.incident_start)-6), 2), 23) as incident_date,
	convert(time, left(right(monitor.incident_start, 6), 2) + ':' + 
	right(left(monitor.incident_start, len(monitor.incident_start)-2), 2) + ':' + 
	right(monitor.incident_start, 2), 8) as incident_time,
	monitor.incident_code,
	monitor.is_sound_alert_on + monitor.is_visual_alert_on as total_alarm,
	monitor.road_segment_id,
	(case when confirm.confirmation is null and 
	ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) > 0 then 'OPEN' else confirm.confirmation end) as confirmation
FROM 
	dbo.tbl_t_monitor as monitor 
left join (select 
id_monitor, confirmation, max(created_date) as created_date
from dbo.tbl_t_confirmation 
group by id_monitor, confirmation) as confirm on monitor.id = confirm.id_monitor
GO
/****** Object:  Table [dbo].[sysdiagrams]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sysdiagrams](
	[name] [sysname] NOT NULL,
	[principal_id] [int] NOT NULL,
	[diagram_id] [int] IDENTITY(1,1) NOT NULL,
	[version] [int] NULL,
	[definition] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_t_message]    Script Date: 19/11/2021 13:53:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_t_message](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[message] [text] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[id_monitor] [int] NULL,
	[incident_code] [varchar](20) NULL,
	[unit_no] [varchar](5) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [mh02_copy] SET  READ_WRITE 
GO
