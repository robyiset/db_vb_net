USE [master]
GO
/****** Object:  Database [db_mh02]    Script Date: 11/11/2021 06:37:14 ******/
CREATE DATABASE [db_mh02]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_mh02', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\db_mh02.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'db_mh02_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQL2016\MSSQL\DATA\db_mh02_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [db_mh02] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_mh02].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_mh02] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_mh02] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_mh02] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_mh02] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_mh02] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_mh02] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_mh02] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_mh02] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_mh02] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_mh02] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_mh02] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_mh02] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_mh02] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_mh02] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_mh02] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_mh02] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_mh02] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_mh02] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_mh02] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_mh02] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_mh02] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_mh02] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_mh02] SET RECOVERY FULL 
GO
ALTER DATABASE [db_mh02] SET  MULTI_USER 
GO
ALTER DATABASE [db_mh02] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_mh02] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_mh02] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_mh02] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_mh02] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [db_mh02] SET QUERY_STORE = OFF
GO
USE [db_mh02]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [db_mh02]
GO
/****** Object:  Table [dbo].[tbl_t_monitor]    Script Date: 11/11/2021 06:37:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_t_monitor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[heart_beat] [float] NULL,
	[timestamp] [varchar](14) NOT NULL,
	[device_id] [varchar](8) NOT NULL,
	[device_ip] [varchar](15) NULL,
	[unit_no] [varchar](5) NOT NULL,
	[incident_code] [varchar](20) NOT NULL,
	[incident_start] [varchar](14) NOT NULL,
	[incident_end] [varchar](14) NULL,
	[is_visual_alert_on] [int] NOT NULL,
	[is_sound_alert_on] [int] NOT NULL,
	[image_file_name] [varchar](255) NULL,
	[video_file_name] [varchar](255) NULL,
	[gps_lat] [float] NULL,
	[gps_long] [float] NULL,
	[road_segment_id] [varchar](10) NOT NULL,
	[hm] [float] NULL,
	[rpm] [float] NULL,
	[vehicle_speed] [float] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
 CONSTRAINT [PK_tbl_t_monitor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_log_pelanggaran]    Script Date: 11/11/2021 06:37:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_log_pelanggaran] as
SELECT
	incident_start,
	left(incident_start, 4) + '-' + 
	right(left(left(incident_start, len(incident_start)-6), len(left(incident_start, len(incident_start)-6))-2), 2) + '-' + 
	right(left(incident_start, len(incident_start)-6), 2) as incident_date,
	left(right(incident_start, 6), 2) + ':' + 
	right(left(incident_start, len(incident_start)-2), 2) + ':' + 
	right(incident_start, 2) as incident_time,
	incident_code,
	(case when ROW_NUMBER() OVER(PARTITION BY unit_no ORDER BY unit_no ASC) = 1 then 'Kesatu' 
	when ROW_NUMBER() OVER(PARTITION BY unit_no ORDER BY unit_no ASC) = 2 then 'Kedua'
	when ROW_NUMBER() OVER(PARTITION BY unit_no ORDER BY unit_no ASC) = 3 then 'Ketiga' else '' end) as frequency_incident,
	unit_no,
	road_segment_id
FROM 
	dbo.tbl_t_monitor;
GO
/****** Object:  Table [dbo].[tbl_t_confirmation]    Script Date: 11/11/2021 06:37:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_t_confirmation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[confirmation] [varchar](15) NOT NULL,
	[catatan] [text] NULL,
	[nama_nrp_gl] [varchar](50) NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[id_monitor] [int] NULL,
 CONSTRAINT [PK_tbl_t_confirmation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_control_monitor_alarm]    Script Date: 11/11/2021 06:37:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_control_monitor_alarm] as
SELECT
	monitor.id,
	monitor.unit_no,
	'PAMA' as organisation,
	monitor.device_id,
	monitor.vehicle_speed,
	(case when ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) = 1 then 'Kesatu' 
	when ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) = 2 then 'Kedua'
	when ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) = 3 then 'Ketiga' else '' end) as frequency_incident,
	left(incident_start, 4) + '-' + 
	right(left(left(incident_start, len(incident_start)-6), len(left(incident_start, len(incident_start)-6))-2), 2) + '-' + 
	right(left(incident_start, len(incident_start)-6), 2) + ' ' +
	left(right(incident_start, 6), 2) + ':' + 
	right(left(incident_start, len(incident_start)-2), 2) + ':' + 
	right(incident_start, 2) as incident_start,
	monitor.incident_code,
	monitor.is_sound_alert_on + monitor.is_visual_alert_on as total_alarm,
	monitor.road_segment_id,
	(case when confirm.confirmation is null and 
	ROW_NUMBER() OVER(PARTITION BY monitor.unit_no ORDER BY monitor.unit_no ASC) > 0 then 'OPEN' else confirm.confirmation end) as confirmation
FROM 
	dbo.tbl_t_monitor as monitor 
left join (select 
id_monitor, confirmation, max(created_date) as created_date
from dbo.tbl_t_confirmation 
group by id_monitor, confirmation) as confirm on monitor.id = confirm.id_monitor;
GO
/****** Object:  Table [dbo].[tbl_t_message]    Script Date: 11/11/2021 06:37:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_t_message](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[message] [text] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [varchar](50) NOT NULL,
	[modified_date] [datetime] NULL,
	[modified_by] [varchar](50) NULL,
	[id_monitor] [int] NULL,
	[incident_code] [varchar](20) NULL,
	[unit_no] [varchar](5) NULL,
 CONSTRAINT [PK_tbl_t_message] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_t_confirmation] ADD  CONSTRAINT [DF_tbl_t_confirmation_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[tbl_t_confirmation] ADD  CONSTRAINT [DF_tbl_t_confirmation_created_by]  DEFAULT ('') FOR [created_by]
GO
ALTER TABLE [dbo].[tbl_t_message] ADD  CONSTRAINT [DF_tbl_t_message_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[tbl_t_message] ADD  CONSTRAINT [DF_tbl_t_message_created_by]  DEFAULT ('') FOR [created_by]
GO
ALTER TABLE [dbo].[tbl_t_message] ADD  CONSTRAINT [DF_tbl_t_message_id_monitor]  DEFAULT ((0)) FOR [id_monitor]
GO
ALTER TABLE [dbo].[tbl_t_monitor] ADD  CONSTRAINT [DF_tbl_t_monitor_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[tbl_t_monitor] ADD  CONSTRAINT [DF_tbl_t_monitor_created_by]  DEFAULT ('') FOR [created_by]
GO
USE [master]
GO
ALTER DATABASE [db_mh02] SET  READ_WRITE 
GO
