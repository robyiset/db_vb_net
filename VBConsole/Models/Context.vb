﻿Imports System.IO
Imports Microsoft.EntityFrameworkCore

Public Class BaseConnection

    Public p_conn_str As String = "Data Source=NDS-LPT-0213\SQL2016;User Id=sa;Password=nawadata"
    Public p_db_one As String = "db_mh02"
    Public p_db_two As String = "mh02_copy"
End Class

Public Class ContextMH02
    Inherits DbContext
    Public Sub New()
    End Sub

    Public Sub New(options As DbContextOptions(Of ContextMH02))
        MyBase.New(options)
        Database.SetCommandTimeout(CInt(TimeSpan.FromMinutes(1).TotalSeconds))
    End Sub

    Public Property tbl_t_monitor As DbSet(Of tbl_t_monitor)
    Public Property tbl_t_confirmation As DbSet(Of tbl_t_confirmation)
    Public Property tbl_t_message As DbSet(Of tbl_t_message)

    Private bc As BaseConnection = New BaseConnection()
    Protected Overrides Sub OnConfiguring(optionsBuilder As DbContextOptionsBuilder)
        optionsBuilder.UseSqlServer(bc.p_conn_str + ";Database=" + bc.p_db_one)
    End Sub

    Protected Overrides Sub OnModelCreating(modelBuilder As ModelBuilder)
        MyBase.OnModelCreating(modelBuilder)
    End Sub

End Class

Public Class ContextMH02_Backup
    Inherits DbContext
    Public Sub New()
    End Sub

    Public Sub New(options As DbContextOptions(Of ContextMH02_Backup))
        MyBase.New(options)
        Database.SetCommandTimeout(CInt(TimeSpan.FromMinutes(1).TotalSeconds))
    End Sub

    Public Property tbl_t_monitor As DbSet(Of tbl_t_monitor)
    Public Property tbl_t_confirmation As DbSet(Of tbl_t_confirmation)
    Public Property tbl_t_message As DbSet(Of tbl_t_message)

    Private bc As BaseConnection = New BaseConnection()
    Protected Overrides Sub OnConfiguring(optionsBuilder As DbContextOptionsBuilder)
        optionsBuilder.UseSqlServer(bc.p_conn_str + ";Database=" + bc.p_db_two)
    End Sub

    Protected Overrides Sub OnModelCreating(modelBuilder As ModelBuilder)
        MyBase.OnModelCreating(modelBuilder)
    End Sub

End Class
