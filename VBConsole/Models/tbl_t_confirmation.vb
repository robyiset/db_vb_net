﻿
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

<Table("tbl_t_confirmation", Schema:="dbo")>
Public Class tbl_t_confirmation
    <Key>
    Public Property id As Integer
    Public Property confirmation As String
    Public Property catatan As String
    Public Property nama_nrp_gl As String
    Public Property created_date As DateTime
    Public Property created_by As String
    Public Property modified_date As DateTime?
    Public Property modified_by As String
    Public Property id_monitor As Integer
End Class