﻿
Imports System.ComponentModel.DataAnnotations.Schema

<Table("tbl_t_message", Schema:="dbo")>
Public Class tbl_t_message
    Public Property id As Integer
    Public Property message As String
    Public Property created_date As DateTime
    Public Property created_by As String
    Public Property modified_date As DateTime?
    Public Property modified_by As String
    Public Property id_monitor As Integer
    Public Property incident_code As String
    Public Property unit_no As String
End Class