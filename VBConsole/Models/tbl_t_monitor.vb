﻿
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

<Table("tbl_t_monitor", Schema:="dbo")>
Public Class tbl_t_monitor
    <Key>
    Public Property id As Integer
    Public Property heart_beat As Double?
    Public Property timestamp As String
    Public Property device_id As String
    Public Property device_ip As String
    Public Property unit_no As String
    Public Property incident_code As String
    Public Property incident_start As String
    Public Property incident_end As String
    Public Property is_visual_alert_on As Integer
    Public Property is_sound_alert_on As Integer
    Public Property image_file_name As String
    Public Property video_file_name As String
    Public Property gps_lat As Double?
    Public Property gps_long As Double?
    Public Property road_segment_id As String
    Public Property hm As Double?
    Public Property rpm As Double?
    Public Property vehicle_speed As Double
    Public Property created_date As DateTime
    Public Property created_by As String
    Public Property modified_date As DateTime?
    Public Property modified_by As String
End Class
